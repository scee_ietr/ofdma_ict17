### OFDMA_ICT17

This repository contains the code which has been used in order to generate the simulation results done for our article: 

Bonnefoi, R.; Moy, C.; Far�s, H.; Palicot, J. �Power Allocation for Minimizing Energy Consumption of OFDMA Downlink with Cell DTx�, International Conference on Telecommunication (ICT) , May 2017.

This article can be found on IEEExplore. A version is also available at: https://hal.archives-ouvertes.fr/hal-01504248

### Short description 

This repository contains two main files:

* The first one : main\_2\_users_allocations. m computes the loss caused by the use of the optimal resource allocation without Cell DTx with the optimal power allocation with Cell DTx. (Figure 2 in our paper)
* The second : main\_2\_users_BABS_ACG.m compares the performance of the power allocation with the BAS+ACG algorithm and the one with the optimal policy. (Figure 3 in our paper)

### License

[MIT Licensed](https://mit-license.org/) (file [LICENSE.txt](LICENSE.txt)).