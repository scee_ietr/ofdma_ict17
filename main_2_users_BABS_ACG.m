%   MAIN_2_BABS_ACG In this file we analyze the performances of the
%   power allocation with and without DTx with the BABS+ACG algorithm.

%   Other m-files required: permn.m
%							BABS_ACG.m 
%                           Biss_mu_min.m
%                           Biss_mu_opt.m
%   Subfunctions: none
%   MAT-files required: none
%
%   See also : none

%   This file is the code used for the simulations done in the paper:
%   Bonnefoi, R.; Moy, C.; Palicot, J. �Power Allocation for Minimizing Energy
%   Consumption of OFDMA Downlink with Cell DTx�, IEEE ICT, May 2017. 

%   The BABS+ACG algorithm was first introduced by Kivanc and al. in:
%   D. Kivanc, Guoqing Li and Hui Liu, "Computationally efficient bandwidth 
%   allocation and power control for OFDMA," in IEEE Transactions on Wireless 
%   Communications, vol. 2, no. 6, pp. 1150-1158, Nov. 2003.

%   Author  : R�mi BONNEFOI
%   SCEE Research Team - CentraleSup�lec - Campus de Rennes
%   Avenue de la Boulaie 35576 Rennes CEDEX CS 47601 FRANCE
%
%   Email : remi.bonnefoi@centralesupelec.fr
%   Website : remibonnefoi.wordpress.com
%
%   Last revision : 07/28/2016

% clear
clear all;
close all;
clc;

%%%%%%%%%%%%%%%%%
% PARAMETERS    %
%%%%%%%%%%%%%%%%%

% Number of iterations per capacity
Nb_iter     = 100;

% Base station %
P0          = 4800;
Ps          = 2900;  
Pmax        = 50;
m           = 8;

% Parameters for fading generation
Nc          = 6;            % Number of channels
CModel      = 'ETU';        % extended piedestran channel model
Ts          = 1/15000;      % Time symbol in LTE
fs          = (12*Nc)/Ts;   % Sampling frequency in Hz, Here we consider Nc RB (Nc*12 subcarriers)
duration    = 2*Ts;         % Duration of the simulation in s
fd          = 0;            % dopler frequency
Bc          = 15000*12;

% users
Nu          = 2;
% Compputation of the average channel gain of the two users
SNR_max_dB  = [15 5];
SNR_max     = 10.^(SNR_max_dB/10);
G           = SNR_max*Bc*Nc/Pmax;
Cap_const1  = 0.1*10^6:0.1*10^6:2*10^6;
frequency   = 2*10^9;

% Average power consumption for each iteration

Power_no    = zeros(1,length(Cap_const1));
Power_opt   = zeros(1,length(Cap_const1));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Iterative resource allocation %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Res_aloc                            = zeros(Nu,Nc);
ind                                 = 1;
vect_temp                           = 1:1:Nu;
res                                 = permn(vect_temp,Nc);

for i=1:1:Nu^Nc
    if sum(res(i,:)==1)>0 && sum(res(i,:)==2)>0,
        Res_aloc(ind,:)     = res(i,:);
        ind                 = ind+1;
    end
end

Nb_alloc                             = size(Res_aloc,1);

%%%%%%%%%%%%%
% Alogrithm %
%%%%%%%%%%%%%

% Mean values %
%%%%%%%%%%%%%%%

Mean_res1               = zeros(1,length(Cap_const1));
Mean_no_DTx             = zeros(1,length(Cap_const1));
Mean_opt                = zeros(1,length(Cap_const1));
Mean_woDTx              = zeros(1,length(Cap_const1));

for ind_cap=1:1:length(Cap_const1)
    
    Cap_const               = Cap_const1(1,ind_cap)*ones(1,Nu);
    Power_cons_noDTx        = zeros(1,Nb_iter);
    Power_cons_res1         = zeros(1,Nb_iter);
    Power_cons_opt          = zeros(1,Nb_iter);
    Power_cons_woDTx        = zeros(1,Nb_iter);
    ind_power               = 1;

    for ind=1:1:Nb_iter
        
        % Computation of the channel coefficients
        Channel_coef        = zeros(Nu,fs*Ts);

        for j=1:1:Nu
            [hMatrix, H]        =  generateChannel(CModel, fs, duration, fd, Ts);
            H_norm              = abs(H(1,:));
            Channel_coef(j,:)   = G(1,j)*H_norm;
        end

        % Average channel gain per resource bloc

        channel_eff          = zeros(Nu,Nc);

        for j=1:1:Nc
           for k=1:1:Nu
                channel_eff(k,j)            = (prod(Channel_coef(k,(j-1)*12+1:j*12)))^(1/12);
           end    
        end
        
    % Resource allocation with the BABS+ACG  algorithm
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    [N_sub,Res_aloc_sub]             = BABS_ACG(channel_eff,Cap_const,Bc,Nc,Nu);
    
    % Computation of the optimal power allocation with the BABS resource
    % allocation and without DTx
    
    channel_eff_used                = zeros(Nu,Nc);
    user1                           = Res_aloc_sub==1;
    channel_eff_used(1,user1)       = channel_eff(1,user1);
    user2                           = Res_aloc_sub==2;
    channel_eff_used(2,user2)       = channel_eff(Nu,user2);

    power_alloc                     = -ones(size(channel_eff_used));
    while min(min(power_alloc))<0,
        power_alloc                     = zeros(size(channel_eff_used));
        % Power allocation for each user
        for j=1:1:Nu
            sup_zero                    = channel_eff_used(j,:)>0;
            % First term of the WF allocation
            Coef                                = 2^(Cap_const(1,j)/(sum(sup_zero)*Bc))/((prod(channel_eff_used(j,sup_zero)))^(1/sum(sup_zero)));
            power_alloc(j,sup_zero)             = Bc*(Coef-1./channel_eff_used(j,sup_zero));
        end
        % set to zero all the channel in whcih the power is lower than zero
        channel_eff_used(power_alloc<0)         = 0;
    end
    
    % We check here if the users can be served by the base station
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    Power_trans_noDTx                           = sum(sum(power_alloc));
    
    if min(N_sub)==0 || Power_trans_noDTx>Pmax,
        
        Power_cons_noDTx        = Power_cons_noDTx(1,1:length(Power_cons_noDTx)-1);
        Power_cons_res1         = Power_cons_res1(1,1:length(Power_cons_res1)-1);
        Power_cons_opt          = Power_cons_opt(1,1:length(Power_cons_opt)-1);
        Power_cons_woDTx        = Power_cons_woDTx(1,1:length(Power_cons_woDTx)-1);
        
    else                % We can serve the users
    
        Power_cons_noDTx(1,ind_power)    = P0+m*Power_trans_noDTx;

        % Computation of the optimal power allocation with this resource
         % allocation 

         channel_eff_used                = zeros(Nu,Nc);
         user1                           = Res_aloc_sub==1;
         channel_eff_used(1,user1)       = channel_eff(1,user1);
         user2                           = Res_aloc_sub==2;
         channel_eff_used(2,user2)       = channel_eff(Nu,user2);

         % Power allocation with this resource allocation
        power_alloc_opt                 = -ones(size(channel_eff_used));
        while min(min(power_alloc_opt))<0,
            power_alloc_opt                         = zeros(size(channel_eff_used));
            % Computation of the service time
            [mu_min]                                = Biss_mu_min(channel_eff_used, Cap_const, Bc, Pmax, 0.00001);
            [mu_opt]                                = Biss_mu_opt(channel_eff_used, Cap_const,mu_min, Bc, m, P0, Ps, 0.00001);
            % Power allocation for each user
            for j=1:1:2
                sup_zero                            = channel_eff_used(j,:)>0;
                % First term of the WF allocation
                Coef                                = 2^(Cap_const(1,j)/(sum(sup_zero)*Bc*mu_opt))/((prod(channel_eff_used(j,sup_zero)))^(1/sum(sup_zero)));
                power_alloc_opt(j,sup_zero)         = Bc*(Coef-1./channel_eff_used(j,sup_zero));
            end
            % set to zero all the channel in whcih the power is lower than zero
            channel_eff_used(power_alloc_opt<0)     = 0;

        end
        Power_trans_res1                = sum(sum(power_alloc_opt));
        mu_opt_res1                     = mu_opt;

        % Average power consumption of the base station
        Power_cons_res1(1,ind_power)    = mu_opt_res1.*(P0+m*Power_trans_res1)+(1-mu_opt_res1).*Ps;

        % Computation of the optimal resource and power allocation with cell
        % DTx
        Power_trans_opt         = zeros(1,Nb_alloc);
        mu_opt_opt              = zeros(1,Nb_alloc);
        for i=1:1:2^Nc-2

            channel_eff_used                = zeros(Nu,Nc);
            user1                           = Res_aloc(i,:)==1;
            channel_eff_used(1,user1)       = channel_eff(1,user1);
            user2                           = Res_aloc(i,:)==2;
            channel_eff_used(2,user2)       = channel_eff(2,user2);

            power_alloc                     = -ones(size(channel_eff_used));
            while min(min(power_alloc))<0,
                power_alloc                     = zeros(size(channel_eff_used));
                % Computation of the service time
                [mu_min]                                = Biss_mu_min(channel_eff_used, Cap_const, Bc, Pmax, 0.00001);
                [mu_opt]                                = Biss_mu_opt(channel_eff_used, Cap_const,mu_min, Bc, m, P0, Ps, 0.00001);
                % Power allocation for each user
                for j=1:1:Nu
                    sup_zero                    = channel_eff_used(j,:)>0;
                    % First term of the WF allocation
                    Coef                                = 2^(Cap_const(1,j)/(sum(sup_zero)*Bc*mu_opt))/((prod(channel_eff_used(j,sup_zero)))^(1/sum(sup_zero)));
                    power_alloc(j,sup_zero)             = Bc*(Coef-1./channel_eff_used(j,sup_zero));
                end
                % set to zero all the channel in whcih the power is lower than zero
                channel_eff_used(power_alloc<0)         = 0;
            end
            Power_trans_opt(1,i)                = sum(sum(power_alloc));
            mu_opt_opt(1,i)                     = mu_opt;
            
        end
        
        % Optimal value
        Power_cons                          = mu_opt_opt.*(P0+m*Power_trans_opt)+(1-mu_opt_opt).*Ps;
        Power_cons_opt(1,ind_power)         = min(Power_cons);

        % Optimal power allocation without cell DTx
        Power_trans     = zeros(1,Nb_alloc);
    
        for i=1:1:2^Nc-2

            channel_eff_used                = zeros(Nu,Nc);
            user1                           = Res_aloc(i,:)==1;
            channel_eff_used(1,user1)       = channel_eff(1,user1);
            user2                           = Res_aloc(i,:)==2;
            channel_eff_used(2,user2)       = channel_eff(Nu,user2);

            power_alloc                     = -ones(size(channel_eff_used));
            while min(min(power_alloc))<0,
                power_alloc                     = zeros(size(channel_eff_used));
                % Power allocation for each user
                for j=1:1:Nu
                    sup_zero                    = channel_eff_used(j,:)>0;
                    % First term of the WF allocation
                    Coef                                = 2^(Cap_const(1,j)/(sum(sup_zero)*Bc))/((prod(channel_eff_used(j,sup_zero)))^(1/sum(sup_zero)));
                    power_alloc(j,sup_zero)             = Bc*(Coef-1./channel_eff_used(j,sup_zero));
                end
                % set to zero all the channel in whcih the power is lower than zero
                channel_eff_used(power_alloc<0)         = 0;
            end
            Power_trans(1,i)            = sum(sum(power_alloc)); 
        end

         % Computation of the optimal power allocation with this resource
         % allocation 

        [M_noDTX, I_noDTX]              = min(Power_trans);
        Power_cons_woDTx(1,ind_power)   = P0+m*M_noDTX;

        ind_power                           = ind_power+1;
    end
    end
    
    % average values for plot
    Mean_res1(1,ind_cap)                = mean(Power_cons_res1);
    Mean_no_DTx(1,ind_cap)              = mean(Power_cons_noDTx);
    Mean_opt(1,ind_cap)                 = mean(Power_cons_opt);
    Mean_woDTx(1,ind_cap)               = mean(Power_cons_woDTx);
end

figure;
hold on;
plot(Cap_const1*10^(-6),Mean_woDTx/1000,'-d','Color',[0.4940    0.1840    0.5560]);
plot(Cap_const1*10^(-6),Mean_no_DTx/1000,'-o','Color',[0.9290    0.6940    0.1250]);
plot(Cap_const1*10^(-6),Mean_res1/1000,'-*','Color',[0    0.4470    0.7410]);
plot(Cap_const1*10^(-6),Mean_opt/1000,'->','Color',[0.6350    0.0780    0.1840]);
legend('Optimal w/o Cell DTx','BABS+ACG w/o Cell DTx', 'BABS+ACG with Cell', 'Optimale with Cell DTx');
xlabel('Capacity constraint (Mbits/s)')
ylabel('Power consumption (W)')
xlim([0.1 1.5])
grid on;
box on;
set(gca,'FontSize',13)

% Computation of the loss
Loss            = (Mean_res1-Mean_opt)./(Mean_opt);