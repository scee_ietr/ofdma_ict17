function [N_sub,Res_aloc_sub]           = BABS_ACG(channel_eff,Cap_const,Bc,Nc,Nu)

%   This function uses the BABS+ACG algorithm to compute the optimal resource
%   allocation.
%   This algorithm has been introduced in :D. Kivanc, Guoqing Li and Hui Liu, 
%   "Computationally efficient bandwidth allocation and power control for OFDMA,"
%   in IEEE Transactions on Wireless Communications, vol. 2, no. 6, pp. 1150-1158, Nov. 2003.

%   Inputs: 
%   channel_eff               : Channel coefficients (linear)
%   Cap_const                 : capacity constraint per user
%   Bc                        : Subchannels bandwidth
%   Nc                        : Number of subchannels
%   Nu                        : Number of users

%   Outputs:
%   Res_aloc_sub              : Suboptimal resource allocation gives the ID
%                             of the user assigned to a channel
%   N_sub                     : Number of subcarriers per user

%   Other m-files required: none
%   Subfunctions: none
%   MAT-files required: none
%
%   See also : none

%   Author  : R�mi BONNEFOI
%   SCEE Research Team - CentraleSup�lec - Campus de Rennes
%   Avenue de la Boulaie 35576 Rennes CEDEX CS 47601 FRANCE
%
%   Email : remi.bonnefoi@centralesupelec.fr
%   Website : remibonnefoi.wordpress.com
%
%   Last revision : 07/28/2016

     % Maximum throughput per subchannel
     SNR_max                    = 50;
     Cmax                       = Bc*log2(1+10^(SNR_max/10));

     % Average Channel gain
     H                          = transpose((1/(Bc*Nc))*sum(channel_eff,2));

     % Number of subcarriers per user
    N_sub                      = ceil(Cap_const/Cmax);

    while sum(N_sub)>Nc,
        [~, I]                  = min(N_sub);
        N_sub(I)                = 0;
    end

    while  sum(N_sub)<Nc,
        Power_dif                 = ((N_sub+1)./H).*(2.^(Cap_const./(Bc*(N_sub+1)))-1)-((N_sub)./H).*(2.^(Cap_const./(Bc*N_sub))-1);
        [~, I]                    = min(Power_dif);
        N_sub(I)                  = N_sub(I)+1;
    end

    % ACG algorithm
    %%%%%%%%%%%%%%%%
    channel_eff_used            = channel_eff;
    Res_aloc_sub                = zeros(1,Nu);

    for i=1:1:Nc

        [~, I]                  = max(channel_eff_used(:,i));

        while sum(Res_aloc_sub==I)==N_sub(I),
            channel_eff_used(I,i)           = 0;
            [~, I]                          = max(channel_eff_used(:,i));
        end
        Res_aloc_sub(1,i)       = I; 

    end
end